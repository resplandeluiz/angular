import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diretivas-customizadas',
  templateUrl: './diretivas-customizadas.component.html',
  styleUrls: ['./diretivas-customizadas.component.css']
})
export class DiretivasCustomizadasComponent implements OnInit {backgroundColor : string = 'yellow';
  constructor() { }

  mostarCursos: boolean = false;

  mostrarCursos() {
    this.mostarCursos = !this.mostarCursos;
  }

  ngOnInit() {
  }



}
