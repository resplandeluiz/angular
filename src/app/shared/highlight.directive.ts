import {Directive, HostBinding, HostListener, Input} from '@angular/core';

@Directive({
    selector: '[appHighlight]'
})
export class HighlightDirective {

    @Input() defaultColor: string = 'white';
    @Input('appHighlight') highlightColor:string = 'yellow'

    @HostListener('mouseenter') onMouserOver() {
        this.backgroundColor = this.highlightColor;
    }

    @HostListener('mouseleave') onMouserLeave() {
        this.backgroundColor = this.defaultColor;
    }

    /*CASO N PRECISE DE MANIPULAÇÃO*/
    @HostBinding('style.backgroundColor') backgroundColor:string;

    constructor() {
    }

    ngOnInit(){
        this.backgroundColor = this.defaultColor;
    }

}
