import { Directive, HostListener, ElementRef, Renderer, HostBinding } from '@angular/core';

@Directive({
  selector: '[appHighlightMouse]'
})
export class HighlightMouseDirective {

  @HostListener('mouseenter') onMouserOver(){
     /* this.renderer.setElementStyle(
      this.elementRef.nativeElement,
      'background-color', 'red');*/
      this.backgroundColor = 'yellow';

  }

  @HostListener('mouseleave') onMouserLeave(){
    /*this.renderer.setElementStyle(
        this.elementRef.nativeElement,
        'background-color', 'white');*/
    this.backgroundColor = 'white';
  }

  /*CASO N PRECISE DE MANIPULAÇÃO*/
 // @HostBinding('style.backgroundColor') backgroundColor:string;

  /*CASO PRECISE MANIPULAR*/
  @HostBinding('style.backgroundColor') get setColor(){

   return this.backgroundColor;
  } private backgroundColor: string;

  constructor() { }

}
