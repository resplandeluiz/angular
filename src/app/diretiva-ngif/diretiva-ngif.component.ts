import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diretiva-ngif',
  templateUrl: './diretiva-ngif.component.html',
  styleUrls: ['./diretiva-ngif.component.css']
})
export class DiretivaNgifComponent implements OnInit {

  cursos: string[] = ['PHP'];
  mostarCursos: boolean = false;
  constructor() { }
  mostrarCursos(){

  this.mostarCursos = !this.mostarCursos;
  console.log(this.mostarCursos);
  }
  ngOnInit() {
  }

}
