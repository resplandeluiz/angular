import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-curso',
  templateUrl: './input-propery.component.html',
  styleUrls: ['./input-propery.component.css']

})
export class InputProperyComponent implements OnInit {

 @Input('nome') nomeCurso: string = '';

  constructor() { }

  ngOnInit() {
  }

}
