import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputProperyComponent } from './input-propery.component';

describe('InputProperyComponent', () => {
  let component: InputProperyComponent;
  let fixture: ComponentFixture<InputProperyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputProperyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputProperyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
